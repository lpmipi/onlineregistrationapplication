1.	CREATE DOCKER with Postgres image
 

 

2.	Once the Postgres Image has been pulled use the following command to startup the container which will point to port 5432 and Container name is set to online-postgres, and the database Username=postgres with Password=0nl1n3.
$ docker run -d -p 5432:5432 --name onlinereg-postgres -e POSTGRES_PASSWORD=0nl1n3 postgres
 
 


3.	Startup your postgres with your container.
 

4.	To access your postgres inside docker container from your local machine(host) execute the below command.

$ docker exec -it onlinereg-postgres bash 
Then login to database with username : psql -U postgres
Create a Database with the below SQL command
# CREATE DATABASE onlineregistration;
 
 


5.	Create Table person as per below.
 
6.	CommandLineRunner Bean is use to insert initial two records to DB and this can be deleted since it is for testing purpose to verify if we are able to access DB. 
 

 

7.	Creating our FrontEnd for Online Registration Application we used Angular and executed the following command.
a.	The application source will be upload on Bitbucket repository. 

 
 

8.	Once the online-registration-application has been downloaded from the repository 
a.	To execute it run the following command to run it with the default port 4200 and navigate to http://localhost:4200/ once its running : ng serve
b.	Please note that this will not serve till the Spring boot application is running.
The Spring Boot API for backend is running on the following http://localhost:8080/api/v1/person




NB: Please refer to Document named: IQBusiness_OnlineRegistrationApplication Setup Doc